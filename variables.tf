variable "bucket" {
  description = "bucket that will host the app"
  type        = string
}

variable "name" {
  description = "Name of your app."
  type        = string
}

variable "environment" {
  description = "Environment where app should be deployed like dev, preprod or prod."
  default     = "dev"
  type        = string
}

// variable "artifact_dir" {
//   description = "Path to your static website"
//   type        = string
// }

variable "index_page" {
  description = "Index page path for your site, e.g: index.html."
  default     = "index.html"
  type        = string
}

variable "error_page" {
  description = "Error page path for your site, e.g: index.html."
  default     = "index.html"
  type        = string
}

variable "enable_versioning" {
  description = "Enable versioning for your S3 bucket to store artifact."
  default     = "Disabled"
  validation {
    condition = contains(["Enabled", "Suspended", "Disabled"], var.enable_versioning)
    error_message = "Allowed values are one of Enabled Suspended Disabled."
  }
}

variable "cert_arn" {
  description = "ARN of the SSL Certificate to use for the Cloudfront Distribution"
  type        = string
}

variable "domain" {
  description = "Your root domain, e.g: example.dev."
}

variable "dom_aliases" {
  description = "alternative domain aliases for cloudfront"
  default     = []
  type        = list(string)
}

variable "cname" {
  description = "Name of CNAME record."
}

variable "custom_origin_config" {
  description = "Define custom bucket settings"
  type        = list(
    object({
      http_port              = number
      https_port             = number
      origin_protocol_policy = string
      origin_ssl_protocols   = list(string)
    })
  )
  default = []
}

variable "error_pages" {
  description = "Define how CloudFront will respond to requests with a 4xx or 5xx error code"
  type        = object({
    error_code            = number
    error_caching_min_ttl = number
    response_code         = number
    response_page_path    = string
  })
  default = null
}

variable "cors_allowed_headers" {
  type        = list(string)
  default     = ["*"]
  description = "List of allowed headers"
}

variable "cors_allowed_methods" {
  type        = list(string)
  default     = ["GET"]
  description = "List of allowed methods (e.g. GET, PUT, POST, DELETE, HEAD) "
}

variable "cors_allowed_origins" {
  type        = list(string)
  default     = ["*"]
  description = "List of allowed origins (e.g. example.com, test.com)"
}

variable "cors_expose_headers" {
  type        = list(string)
  default     = ["ETag"]
  description = "List of expose header in the response"
}

variable "cors_max_age_seconds" {
  type        = number
  default     = 3600
  description = "Time in seconds that browser can cache the response"
}

variable "bucket_acl" {
  type = string
  default = "private"
  description = "The canned ACL to apply to the bucket. More info: https://docs.aws.amazon.com/AmazonS3/latest/userguide/acl-overview.html"
  validation {
    condition = contains(["private", "public-read", "public-read-write", "aws-exec-read", "authenticated-read", "log-delivery-write"], var.bucket_acl)
    error_message = "Allowed values are one of private, public-read, public-read-write, aws-exec-read, authenticated-read or log-delivery-write."
  }
}
