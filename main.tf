# Create S3 bucket for site
resource "aws_s3_bucket" "site_bucket" {
  bucket = var.bucket

  tags = {
    APP   = var.name
    STAGE = var.environment
  }
}

resource "aws_s3_bucket_ownership_controls" "site_bucket" {
  bucket = aws_s3_bucket.site_bucket.id
  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_s3_bucket_versioning" "site_bucket" {
  bucket = aws_s3_bucket.site_bucket.id
  versioning_configuration {
    status = var.enable_versioning
  }
}

resource "aws_s3_bucket_website_configuration" "site_bucket" {
  bucket = aws_s3_bucket.site_bucket.bucket

  index_document {
    suffix = var.index_page
  }

  error_document {
    key = var.error_page
  }
}

resource "aws_s3_bucket_cors_configuration" "site_bucket" {
  bucket = aws_s3_bucket.site_bucket.bucket

  cors_rule {
    allowed_headers = var.cors_allowed_headers
    allowed_methods = var.cors_allowed_methods
    allowed_origins = var.cors_allowed_origins
    expose_headers  = var.cors_expose_headers
    max_age_seconds = var.cors_max_age_seconds
  }
}

# Create new ACM if no cert_arn is provided
#provider "aws" {
#  # As https between viewers and cloudfront require certs to be issued in us-east-1
#  alias  = "virginia"
#  region = "us-east-1"
#}
#resource "aws_acm_certificate" "cert" {
#  count    = var.cert_arn == "" ? 1 : 0
#  provider = aws.virginia
#
#  domain_name       = "*.${var.domain}"
#  validation_method = "DNS"
#
#  tags = {
#    Environment = var.environment
#  }
#
#  lifecycle {
#    create_before_destroy = true
#  }
#}
#
#resource "aws_acm_certificate_validation" "cert" {
#  provider        = aws.virginia
#  certificate_arn = var.cert_arn == "" ? aws_acm_certificate.cert[0].arn : var.cert_arn
#  //     validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
#}

# CloudFront configuration for the web page
resource "aws_cloudfront_distribution" "cdn" {
  comment = "Managed by Terraform"
  origin {
    domain_name = aws_s3_bucket.site_bucket.bucket_regional_domain_name
    origin_id   = var.cname

    dynamic "custom_origin_config" {
      for_each = [for s in var.custom_origin_config : {
        http_port              = lookup(s, "http_port", "")
        https_port             = lookup(s, "https_port", "")
        origin_protocol_policy = lookup(s, "origin_protocol_policy", "")
        origin_ssl_protocols   = lookup(s, "origin_ssl_protocols", "")
      }]

      content {
        http_port              = custom_origin_config.value.http_port
        https_port             = custom_origin_config.value.https_port
        origin_protocol_policy = custom_origin_config.value.origin_protocol_policy
        origin_ssl_protocols   = custom_origin_config.value.origin_ssl_protocols
      }
    }

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  default_root_object = "index.html"

  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = var.cname
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  aliases = concat(["${var.cname}.${var.domain}"], var.dom_aliases)

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    #acm_certificate_arn = var.cert_arn == "" ? aws_acm_certificate_validation.cert.certificate_arn : var.cert_arn
    acm_certificate_arn = var.cert_arn
    ssl_support_method  = "sni-only"
    minimum_protocol_version = "TLSv1.2_2021"
  }

  # Add [Custom Error Response Arguments](https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html#custom-error-response-arguments)
  dynamic "custom_error_response" {
    for_each = var.error_pages != null ? ["true"] : []
    content {
      error_code            = var.error_pages.error_code
      error_caching_min_ttl = var.error_pages.error_caching_min_ttl
      response_code         = var.error_pages.response_code
      response_page_path    = var.error_pages.response_page_path
    }
  }
}

# Create CloudFront OAI (Origin Access Identity) to allow access into the S3 origin
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "Managed by Terraform"
}

# Create the policy to apply to S3 origin bucket
data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.site_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.site_bucket.arn]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_site_policy" {
  bucket = aws_s3_bucket.site_bucket.id
  policy = data.aws_iam_policy_document.s3_policy.json
}
