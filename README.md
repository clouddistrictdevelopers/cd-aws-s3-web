## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudfront_distribution.cdn](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | resource |
| [aws_cloudfront_origin_access_identity.origin_access_identity](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_origin_access_identity) | resource |
| [aws_s3_bucket.site_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.bucket_site_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_iam_policy_document.s3_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket"></a> [bucket](#input\_bucket) | bucket that will host the app | `string` | n/a | yes |
| <a name="input_cert_arn"></a> [cert\_arn](#input\_cert\_arn) | ARN of the SSL Certificate to use for the Cloudfront Distribution | `string` | n/a | yes |
| <a name="input_cname"></a> [cname](#input\_cname) | Name of CNAME record. | `any` | n/a | yes |
| <a name="input_dom_aliases"></a> [dom\_aliases](#input\_dom\_aliases) | alternative domain aliases for cloudfront | `list(string)` | `[]` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | Your root domain, e.g: example.dev. | `any` | n/a | yes |
| <a name="input_enable_versioning"></a> [enable\_versioning](#input\_enable\_versioning) | Enable versioning for your S3 bucket to store artifact. | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment where app should be deployed like dev, preprod or prod. | `string` | `"dev"` | no |
| <a name="input_error_page"></a> [error\_page](#input\_error\_page) | Error page path for your site, e.g: index.html. | `string` | `"index.html"` | no |
| <a name="input_error_pages"></a> [error\_pages](#input\_error\_pages) | Define how CloudFront will respond to requests with a 4xx or 5xx error code | <pre>object({<br>    error_code            = number<br>    error_caching_min_ttl = number<br>    response_code         = number<br>    response_page_path    = string<br>  })</pre> | `null` | no |
| <a name="input_index_page"></a> [index\_page](#input\_index\_page) | Index page path for your site, e.g: index.html. | `string` | `"index.html"` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of your app. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cdn_domain_name"></a> [cdn\_domain\_name](#output\_cdn\_domain\_name) | Domain name of the Cloudfront Distribution |
| <a name="output_cdn_oai_arn"></a> [cdn\_oai\_arn](#output\_cdn\_oai\_arn) | A pre-generated ARN for use in S3 bucket policies |
| <a name="output_cdn_oai_id"></a> [cdn\_oai\_id](#output\_cdn\_oai\_id) | The identifier for the distribution |
| <a name="output_website_bucket_arn"></a> [website\_bucket\_arn](#output\_website\_bucket\_arn) | n/a |
| <a name="output_website_bucket_id"></a> [website\_bucket\_id](#output\_website\_bucket\_id) | n/a |
| <a name="output_website_domain"></a> [website\_domain](#output\_website\_domain) | n/a |
| <a name="output_website_endpoint"></a> [website\_endpoint](#output\_website\_endpoint) | n/a |
