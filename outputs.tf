// output "website_url" {
//   value = "https://${var.cname}.${var.domain}"
// }

output "website_bucket_id" {
    value = aws_s3_bucket.site_bucket.id
}

output "website_bucket_arn" {
    value = aws_s3_bucket.site_bucket.arn
}

output "website_bucket_region" {
    value = aws_s3_bucket.site_bucket.region
}

output "website_endpoint" {
    value = aws_s3_bucket.site_bucket.website_endpoint
}

output "website_domain" {
    value = aws_s3_bucket.site_bucket.website_domain
}

# ACM Certificate
#output "web_cert_arn" {
#    value = "${var.cert_arn == "" ? aws_acm_certificate.cert[0].arn : var.cert_arn }"
#}
#output "web_cert_record" {
#    // value    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
#    value    = aws_acm_certificate.cert
#}
#output "web_validated_cert_arn" {
#    value = "${aws_acm_certificate_validation.cert.certificate_arn}"
#}

output "cdn_id" {
  description = "ID of the Cloudfront Distribution"
  value = aws_cloudfront_distribution.cdn.id
}
output "cdn_domain_name" {
  description = "Domain name of the Cloudfront Distribution"
  value       = aws_cloudfront_distribution.cdn.domain_name
}
output "cdn_oai_id" {
    description = "The identifier for the distribution"
    value       = aws_cloudfront_origin_access_identity.origin_access_identity.id
}
output "cdn_oai_arn" {
    description = "A pre-generated ARN for use in S3 bucket policies"
    value       = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
}
